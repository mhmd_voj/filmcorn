package com.project.farjad.filmcorn.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.project.farjad.filmcorn.adapter.VerticalSerialsAdapter;
import com.project.farjad.filmcorn.adapter.onSerialsHorItemListener;
import com.project.farjad.filmcorn.databinding.ActivitySerialsListBinding;
import com.project.farjad.filmcorn.model.Serial;
import com.project.farjad.filmcorn.viewModel.TvShowViewModel;

import java.util.ArrayList;
import java.util.List;

public class SerialsListActivity extends AppCompatActivity implements onSerialsHorItemListener {
    private ActivitySerialsListBinding binding;
    private TvShowViewModel viewModel;
    private int currentPage = 1;
    private int totalAvailablePages = 1;
    private VerticalSerialsAdapter adapter;
    private List<Serial> serials = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySerialsListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        iniViews();

        initRecyclerView();

        getMostPopularSerials();

        binding.btnBackSerialList.setOnClickListener(v -> {
            onBackPressed();
        });
    }

    private void iniViews() {
        viewModel = new ViewModelProvider(this).get(TvShowViewModel.class);
        adapter = new VerticalSerialsAdapter(serials,this);
    }

    private void getMostPopularSerials() {
        loadingManager(true);
        viewModel.getTvShows(currentPage).observe(this, serialListResponse -> {
            totalAvailablePages = serialListResponse.getTotalPages();
            int oldCount = serials.size();
            serials.addAll(serialListResponse.getSerials());
            adapter.notifyItemRangeChanged(oldCount,serials.size());
            loadingManager(false);
        });
    }

    private void initRecyclerView() {
        binding.rclListSerials.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL,false));
        binding.rclListSerials.setAdapter(adapter);
        binding.rclListSerials.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!binding.rclListSerials.canScrollVertically(1)){
                    if (currentPage <= totalAvailablePages){
                        currentPage++;
                        getMostPopularSerials();
                    }
                }
            }
        });
        binding.prgListSerials.setVisibility(View.GONE);

    }

    public void loadingManager(boolean state){
        if (currentPage==1){
            if (state){
                binding.prgListSerials.setVisibility(View.VISIBLE);
            }else {
                binding.prgListSerials.setVisibility(View.GONE);
            }
        }else {
            if (state){
                binding.prgMoreListSerials.setVisibility(View.VISIBLE);
            }else {
                binding.prgMoreListSerials.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClicked(Serial serial) {
        Intent intent = new Intent(this,DetailActivity.class);
        intent.putExtra("idSerial",serial.getId());
        startActivity(intent);
    }
}