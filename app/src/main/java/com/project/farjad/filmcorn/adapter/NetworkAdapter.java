package com.project.farjad.filmcorn.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.model.Networks;

import java.util.List;

public class NetworkAdapter extends RecyclerView.Adapter<NetworkAdapter.NetworkViewHolder> {
    private List<Networks> networksList;

    public NetworkAdapter(List<Networks> networksList) {
        this.networksList = networksList;
    }

    @NonNull
    @Override
    public NetworkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NetworkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.network_item_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull NetworkViewHolder holder, int position) {
        holder.img_network.setImageURI(networksList.get(position).imageUrl);
    }

    @Override
    public int getItemCount() {
        return networksList.size();
    }

    class NetworkViewHolder extends RecyclerView.ViewHolder{
        private SimpleDraweeView img_network;
        public NetworkViewHolder(@NonNull View itemView) {
            super(itemView);
            img_network = itemView.findViewById(R.id.img_network);
        }
    }
}
