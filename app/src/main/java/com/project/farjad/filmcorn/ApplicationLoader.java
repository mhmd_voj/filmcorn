package com.project.farjad.filmcorn;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import io.reactivex.plugins.RxJavaPlugins;

public class ApplicationLoader extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(getApplicationContext());
        RxJavaPlugins.setErrorHandler(throwable -> {}); // nothing or some logging
    }
}
