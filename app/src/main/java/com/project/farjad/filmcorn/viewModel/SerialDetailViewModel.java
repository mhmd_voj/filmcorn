package com.project.farjad.filmcorn.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.project.farjad.filmcorn.model.SerialDetail;
import com.project.farjad.filmcorn.model.SerialDetailResponse;
import com.project.farjad.filmcorn.repositories.SerialDetailRepository;

public class SerialDetailViewModel extends ViewModel {
    private SerialDetailRepository repository;

    public SerialDetailViewModel() {
        repository = new SerialDetailRepository();
    }

    public LiveData<SerialDetailResponse> getSerialDetail(String id){
        return repository.getSerialFDetailsLiveData(id);
    }
}
