package com.project.farjad.filmcorn.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.adapter.VerticalSerialsAdapter;
import com.project.farjad.filmcorn.adapter.onSerialsHorItemListener;
import com.project.farjad.filmcorn.databinding.FragmentSearchBinding;
import com.project.farjad.filmcorn.model.Serial;
import com.project.farjad.filmcorn.viewModel.TvShowViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class FragmentSearch extends Fragment implements onSerialsHorItemListener {
    private FragmentSearchBinding binding;
    private TvShowViewModel viewModel;
    private List<Serial> serialList=new ArrayList<>();
    private VerticalSerialsAdapter adapter;
    private int currentPage = 1;
    private int totalAvailablePages = 1;
    private Timer timer;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSearchBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(TvShowViewModel.class);
        adapter = new VerticalSerialsAdapter(serialList,this);

        initRecyclerView();

        binding.edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (timer!=null)
                    timer.cancel();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().trim().isEmpty()){
                    loadingManager(true);
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            new Handler(Looper.getMainLooper()).post(()->{
                               currentPage =1;
                               totalAvailablePages = 1;
                               getSerials(editable.toString());
                            });
                        }
                    },800);
                }else {
                    serialList.clear();
                    adapter.notifyDataSetChanged();
                    binding.imgSearchFragment.setVisibility(View.VISIBLE);
                    loadingManager(false);
                }
            }
        });
    }

    private void initRecyclerView() {
        binding.rclSearchSerials.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL,false));
        binding.rclSearchSerials.setAdapter(adapter);
        binding.prgSearch.setVisibility(View.GONE);
    }

    private void getSerials(String str) {
        serialList.clear();
        adapter.notifyDataSetChanged();
        loadingManager(true);
        binding.imgSearchFragment.setVisibility(View.GONE);
        viewModel.getSearchTvShow(str,currentPage).observe(this, serialListResponse -> {
            totalAvailablePages = serialListResponse.getTotalPages();
            int oldCount = serialList.size();
            serialList.addAll(serialListResponse.getSerials());
            adapter.notifyItemRangeChanged(oldCount,serialList.size());
            loadingManager(false);
        });
    }

    public void loadingManager(boolean state){
        if (currentPage==1){
            if (state){
                binding.prgSearch.setVisibility(View.VISIBLE);

            }else {
                binding.prgSearch.setVisibility(View.GONE);
            }
        }else {
//            if (state){
//                binding.prgMoreListSerials.setVisibility(View.VISIBLE);
//            }else {
//                binding.prgMoreListSerials.setVisibility(View.GONE);
//            }
        }
    }

    @Override
    public void onClicked(Serial serial) {
        Intent intent = new Intent(getContext(),DetailActivity.class);
        intent.putExtra("idSerial",serial.getId());
        startActivity(intent);
    }
}
