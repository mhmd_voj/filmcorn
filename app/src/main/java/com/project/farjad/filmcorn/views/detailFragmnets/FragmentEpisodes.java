package com.project.farjad.filmcorn.views.detailFragmnets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.adapter.EpisodesAdapter;
import com.project.farjad.filmcorn.model.SerialDetail;

public class FragmentEpisodes extends Fragment {
    private RecyclerView rcl_episodes;
    private EpisodesAdapter episodesAdapter;
    private SerialDetail serialDetail;


    public FragmentEpisodes(SerialDetail serialDetail) {
        this.serialDetail = serialDetail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(getContext()).inflate(R.layout.fragment_episode,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rcl_episodes = view.findViewById(R.id.rcl_episodes);

        episodesAdapter = new EpisodesAdapter(serialDetail.getEpisodes());
        rcl_episodes.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        rcl_episodes.setAdapter(episodesAdapter);
    }
}
