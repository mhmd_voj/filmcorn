
package com.project.farjad.filmcorn.model;

import com.google.gson.annotations.SerializedName;


public class Episode {

    @SerializedName("air_date")
    private String mAirDate;
    @SerializedName("episode")
    private Long mEpisode;
    @SerializedName("name")
    private String mName;
    @SerializedName("season")
    private Long mSeason;

    public String getAirDate() {
        return mAirDate;
    }

    public void setAirDate(String airDate) {
        mAirDate = airDate;
    }

    public Long getEpisode() {
        return mEpisode;
    }

    public void setEpisode(Long episode) {
        mEpisode = episode;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Long getSeason() {
        return mSeason;
    }

    public void setSeason(Long season) {
        mSeason = season;
    }

}
