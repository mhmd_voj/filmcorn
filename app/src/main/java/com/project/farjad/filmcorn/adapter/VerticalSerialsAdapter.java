package com.project.farjad.filmcorn.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.model.Serial;

import java.util.List;

public class VerticalSerialsAdapter extends RecyclerView.Adapter<VerticalSerialsAdapter.VerticalSerialsViewHolder> {
    private List<Serial> serialList;
    private onSerialsHorItemListener listener;

    public VerticalSerialsAdapter(List<Serial> serialList, onSerialsHorItemListener listener) {
        this.serialList = serialList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VerticalSerialsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VerticalSerialsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_item_layout_ver, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VerticalSerialsViewHolder holder, int position) {
        holder.bindSerial(serialList.get(position));
    }

    @Override
    public int getItemCount() {
        return serialList.size();
    }

    public class VerticalSerialsViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_name_serial_list;
        private TextView txt_sate_serial_list;
        private TextView txt_network_serial_list;
        private SimpleDraweeView img_poster_serial_list;
        private ImageView btn_save_favorite_serial;

        public VerticalSerialsViewHolder(@NonNull View itemView) {
            super(itemView);
            btn_save_favorite_serial = itemView.findViewById(R.id.btn_save_favorite_serial);
            txt_name_serial_list = itemView.findViewById(R.id.txt_name_serial_list);
            txt_sate_serial_list = itemView.findViewById(R.id.txt_sate_serial_list);
            txt_network_serial_list = itemView.findViewById(R.id.txt_network_serial_list);
            img_poster_serial_list = itemView.findViewById(R.id.img_poster_serial_list);
        }

        public void bindSerial(Serial serial) {
            txt_name_serial_list.setText(serial.getName());
            txt_sate_serial_list.setText("Start on " + serial.getStartDate() + " and " + serial.getStatus());
            txt_network_serial_list.setText(serial.getNetwork());
            img_poster_serial_list.setImageURI(serial.getImageThumbnailPath());
            btn_save_favorite_serial.setOnClickListener(view -> {

            });
            itemView.setOnClickListener(view -> listener.onClicked(serial));
        }
    }
}
