package com.project.farjad.filmcorn.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.model.Serial;

import java.util.List;

public class HorizontalSerialsAdapter extends RecyclerView.Adapter<HorizontalSerialsAdapter.HorizontalSerialsViewHolder> {
    private List<Serial> serialList;
    private onSerialsHorItemListener listener;

    public HorizontalSerialsAdapter(List<Serial> serialList, onSerialsHorItemListener listener) {
        this.serialList = serialList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public HorizontalSerialsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HorizontalSerialsViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.movies_item_layout_hor,parent,false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull HorizontalSerialsViewHolder holder, int position) {
        holder.bindSerials(serialList.get(position));
    }

    @Override
    public int getItemCount() {
        return serialList.size();
    }

    public class HorizontalSerialsViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_name_serial_hor;
        private TextView txt_sate_serial_hor;
        private SimpleDraweeView img_poster_serial_list_hor;

        public HorizontalSerialsViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_name_serial_hor = itemView.findViewById(R.id.txt_name_serial_hor);
            txt_sate_serial_hor = itemView.findViewById(R.id.txt_sate_serial_hor);
            img_poster_serial_list_hor = itemView.findViewById(R.id.img_poster_serial_detail);
        }

        public void bindSerials(Serial serial){
            txt_name_serial_hor.setText(serial.getName());
            txt_sate_serial_hor.setText(serial.getStatus());
            img_poster_serial_list_hor.setImageURI(serial.getImageThumbnailPath());

            itemView.setOnClickListener(view -> listener.onClicked(serial));
        }
    }


}
