package com.project.farjad.filmcorn.views;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager2.widget.ViewPager2;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.adapter.FragmentAdapter;
import com.project.farjad.filmcorn.model.SerialDetail;
import com.project.farjad.filmcorn.model.SerialDetailResponse;
import com.project.farjad.filmcorn.viewModel.SerialDetailViewModel;

import jp.wasabeef.blurry.Blurry;

public class DetailActivity extends AppCompatActivity {
    private SerialDetailViewModel viewModel;
    private ViewPager2 viewPagerDetail;
    private TabLayout tabLayoutDetail;
    private  FragmentAdapter fragmentAdapter;
    private ProgressBar prg_detail_movie;
    private SimpleDraweeView img_poster_serial_detail;
    private SimpleDraweeView img_back_serial;
    private TextView txt_name_serial_detail;
    private TextView txt_state_serial_detail;
    private TextView txt_rating_serial_detail;
    private TextView txt_network_serial_detail;
    private TextView txt_time_serial_detail;
    private TextView txt_country_serial_detail;
    private ImageView btn_back_detail;
    private ViewGroup root_detail;
    private long serialID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        viewModel = new ViewModelProvider(this).get(SerialDetailViewModel.class);

        getSerialData();

        initViews();



        btn_back_detail.setOnClickListener(v->{
            onBackPressed();
        });

    }

    private void initViews() {
        viewPagerDetail = findViewById(R.id.view_pager_detail);
        tabLayoutDetail = findViewById(R.id.tab_layout_detail);
        img_poster_serial_detail = findViewById(R.id.img_poster_serial_detail);
        txt_name_serial_detail = findViewById(R.id.txt_name_serial_detail);
        txt_state_serial_detail = findViewById(R.id.txt_state_serial_detail);
        txt_rating_serial_detail = findViewById(R.id.txt_rating_serial_detail);
        txt_network_serial_detail = findViewById(R.id.txt_network_serial_detail);
        txt_time_serial_detail = findViewById(R.id.txt_time_serial_detail);
        prg_detail_movie = findViewById(R.id.prg_detail_movie);
        root_detail = findViewById(R.id.root_detail);
        txt_country_serial_detail = findViewById(R.id.txt_country_serial_detail);
        btn_back_detail = findViewById(R.id.btn_back_detail);
        img_back_serial = findViewById(R.id.img_back_serial);


    }

    private void getSerialData() {
        serialID = getIntent().getLongExtra("idSerial", 0);
        if (serialID > 0) {
            viewModel.getSerialDetail(String.valueOf(serialID)).observe(this, serialDetailResponse -> {
                bindSerialData(serialDetailResponse.getSerialDetail());
                initTabLayout(serialDetailResponse.getSerialDetail());
            });
        }

    }

    private void initTabLayout(SerialDetail serialDetail) {
        fragmentAdapter = new FragmentAdapter(this,serialDetail);
        viewPagerDetail.setAdapter(fragmentAdapter);
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayoutDetail, viewPagerDetail, (tab, position) -> {
            switch (position){
                case 0:
                    tab.setText("Summery");
                    break;
                case 1:
                    tab.setText("Episodes");
                    break;
                case 2:
                    tab.setText("Pictures");
                    break;
            }
        });
        tabLayoutMediator.attach();
    }

    private void bindSerialData(SerialDetail serialDetail) {
        img_poster_serial_detail.setImageURI(serialDetail.getImagePath());
        img_back_serial.setImageURI(serialDetail.getImagePath());
        txt_name_serial_detail.setText(serialDetail.getName());
        txt_country_serial_detail.setText(serialDetail.getCountry());
        txt_network_serial_detail.setText(serialDetail.getNetwork());
        txt_time_serial_detail.setText(serialDetail.getRuntime() + " min");
        txt_rating_serial_detail.setText(serialDetail.getRating().substring(0,3) + "/10 from " +serialDetail.getRatingCount() + "votes");
        txt_state_serial_detail.setText("Start on "+ serialDetail.getStartDate() + " and "+serialDetail.getStatus());
        prg_detail_movie.setVisibility(View.GONE);
        root_detail.setVisibility(View.VISIBLE);
    }
}