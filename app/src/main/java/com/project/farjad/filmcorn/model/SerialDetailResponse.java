package com.project.farjad.filmcorn.model;

import com.google.gson.annotations.SerializedName;

public class SerialDetailResponse {

    @SerializedName("tvShow")
    private SerialDetail serialDetail;

    public SerialDetail getSerialDetail() {
        return serialDetail;
    }
}
