package com.project.farjad.filmcorn.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.project.farjad.filmcorn.model.SerialListResponse;
import com.project.farjad.filmcorn.repositories.SerialsRepository;

public class TvShowViewModel extends ViewModel {
    private SerialsRepository serialsRepository;

    public TvShowViewModel() {
        serialsRepository = new SerialsRepository();
    }

    public LiveData<SerialListResponse> getTvShows(int page){
        return serialsRepository.getTvShows(page);
    }

    public LiveData<SerialListResponse> getSearchTvShow(String string,int page){
        return serialsRepository.getSearchTvShow(string,page);
    }
}
