package com.project.farjad.filmcorn.network;

import com.project.farjad.filmcorn.model.SerialDetailResponse;
import com.project.farjad.filmcorn.model.SerialListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitService {


    @GET("most-popular")
    Single<SerialListResponse> getMostPopularTvShows(@Query("page") int page);

    @GET("show-details")
    Single<SerialDetailResponse> getSerialDetail(@Query("q") String serialId);

    @GET("search")
    Single<SerialListResponse> getSearchTvShows(@Query("q") String query,@Query("page")int page);
}
