package com.project.farjad.filmcorn.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.project.farjad.filmcorn.model.SerialDetail;
import com.project.farjad.filmcorn.views.detailFragmnets.FragmentEpisodes;
import com.project.farjad.filmcorn.views.detailFragmnets.FragmentPictures;
import com.project.farjad.filmcorn.views.detailFragmnets.FragmentSummery;

public class FragmentAdapter extends FragmentStateAdapter {
    private SerialDetail serialDetail;


    public FragmentAdapter(@NonNull FragmentActivity fragmentActivity, SerialDetail serialDetail) {
        super(fragmentActivity);
        this.serialDetail =serialDetail;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 1:
                return new FragmentEpisodes(serialDetail);
            case 2:
                return new FragmentPictures(serialDetail);
            default: return new FragmentSummery(serialDetail);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
