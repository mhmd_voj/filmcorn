package com.project.farjad.filmcorn.views.detailFragmnets;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.adapter.PicturesAdapter;
import com.project.farjad.filmcorn.model.SerialDetail;

public class FragmentPictures extends Fragment {
    private RecyclerView rcl_pictures;
    private PicturesAdapter adapter;
    private SerialDetail serialDetail;

    public FragmentPictures(SerialDetail serialID) {
        serialDetail = serialID;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(getContext()).inflate(R.layout.fragment_pic,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rcl_pictures = view.findViewById(R.id.rcl_pictures);

        adapter =new PicturesAdapter(serialDetail.getPictures());
        rcl_pictures.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        rcl_pictures.setAdapter(adapter);
    }
}
