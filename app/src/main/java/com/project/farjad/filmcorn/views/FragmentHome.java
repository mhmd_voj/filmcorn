package com.project.farjad.filmcorn.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.filmcorn.adapter.HorizontalSerialsAdapter;
import com.project.farjad.filmcorn.adapter.NetworkAdapter;
import com.project.farjad.filmcorn.adapter.onSerialsHorItemListener;
import com.project.farjad.filmcorn.databinding.FragmentHomeBinding;
import com.project.farjad.filmcorn.model.Networks;
import com.project.farjad.filmcorn.model.Serial;
import com.project.farjad.filmcorn.viewModel.TvShowViewModel;

import java.util.List;

public class FragmentHome extends Fragment {
    private FragmentHomeBinding binding;
    private TvShowViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding= FragmentHomeBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel =new ViewModelProvider(this).get(TvShowViewModel.class);

        viewModel.getTvShows(0).observe(getViewLifecycleOwner(), tvShowResponse ->
                initPopularRecyclerViews(tvShowResponse.getSerials()));

        binding.btnShowMorePopular.setOnClickListener(view1 -> {
            Intent intent = new Intent(getContext(),SerialsListActivity.class);
            intent.putExtra("key","popular");
            startActivity(intent);
        });

        initNetworkRecyclerViews();
    }

    private void initPopularRecyclerViews(List<Serial> serials){
        HorizontalSerialsAdapter adapter = new HorizontalSerialsAdapter(serials, serial -> {
            Intent intent = new Intent(getContext(),DetailActivity.class);
            intent.putExtra("idSerial",serial.getId());
            startActivity(intent);
        });
        binding.rclPopularMain.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL,false));
        binding.rclPopularMain.setAdapter(adapter);
        binding.prgPopularMain.setVisibility(View.GONE);
    }

    private void initFavoritesRecyclerViews(List<Serial> serials){
        //HorizontalSerialsAdapter adapter = new HorizontalSerialsAdapter(serials);
        binding.rclFavoritesMain.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL,false));
        //binding.rclFavoritesMain.setAdapter(adapter);
    }

    private void initNetworkRecyclerViews(){
        NetworkAdapter networkAdapter = new NetworkAdapter(Networks.getNetwork().subList(0,6));
        binding.rclNetworksMain.setLayoutManager(new GridLayoutManager(getContext(),3));
        binding.rclNetworksMain.setAdapter(networkAdapter);
        binding.prgNetworksMain.setVisibility(View.GONE);
    }


}
