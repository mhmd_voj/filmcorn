
package com.project.farjad.filmcorn.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SerialDetail {

    @SerializedName("countdown")
    private Object mCountdown;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("description_source")
    private String mDescriptionSource;
    @SerializedName("end_date")
    private Object mEndDate;
    @SerializedName("episodes")
    private List<Episode> mEpisodes;
    @SerializedName("genres")
    private List<String> mGenres;
    @SerializedName("id")
    private Long mId;
    @SerializedName("image_path")
    private String mImagePath;
    @SerializedName("image_thumbnail_path")
    private String mImageThumbnailPath;
    @SerializedName("name")
    private String mName;
    @SerializedName("network")
    private String mNetwork;
    @SerializedName("permalink")
    private String mPermalink;
    @SerializedName("pictures")
    private List<String> mPictures;
    @SerializedName("rating")
    private String mRating;
    @SerializedName("rating_count")
    private String mRatingCount;
    @SerializedName("runtime")
    private Long mRuntime;
    @SerializedName("start_date")
    private String mStartDate;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("youtube_link")
    private Object mYoutubeLink;

    public Object getCountdown() {
        return mCountdown;
    }

    public void setCountdown(Object countdown) {
        mCountdown = countdown;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescriptionSource() {
        return mDescriptionSource;
    }

    public void setDescriptionSource(String descriptionSource) {
        mDescriptionSource = descriptionSource;
    }



    public Object getEndDate() {
        return mEndDate;
    }

    public void setEndDate(Object endDate) {
        mEndDate = endDate;
    }

    public List<Episode> getEpisodes() {
        return mEpisodes;
    }

    public void setEpisodes(List<Episode> episodes) {
        mEpisodes = episodes;
    }

    public List<String> getGenres() {
        return mGenres;
    }

    public void setGenres(List<String> genres) {
        mGenres = genres;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    public String getImageThumbnailPath() {
        return mImageThumbnailPath;
    }

    public void setImageThumbnailPath(String imageThumbnailPath) {
        mImageThumbnailPath = imageThumbnailPath;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNetwork() {
        return mNetwork;
    }

    public void setNetwork(String network) {
        mNetwork = network;
    }

    public String getPermalink() {
        return mPermalink;
    }

    public void setPermalink(String permalink) {
        mPermalink = permalink;
    }

    public List<String> getPictures() {
        return mPictures;
    }

    public void setPictures(List<String> pictures) {
        mPictures = pictures;
    }

    public String getRating() {
        return mRating;
    }

    public void setRating(String rating) {
        mRating = rating;
    }

    public String getRatingCount() {
        return mRatingCount;
    }

    public void setRatingCount(String ratingCount) {
        mRatingCount = ratingCount;
    }

    public Long getRuntime() {
        return mRuntime;
    }

    public void setRuntime(Long runtime) {
        mRuntime = runtime;
    }

    public String getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String startDate) {
        mStartDate = startDate;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public Object getYoutubeLink() {
        return mYoutubeLink;
    }

    public void setYoutubeLink(Object youtubeLink) {
        mYoutubeLink = youtubeLink;
    }



}
