package com.project.farjad.filmcorn.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.model.Episode;

import java.util.List;

public class EpisodesAdapter extends RecyclerView.Adapter<EpisodesAdapter.EpisodesViewHolder> {
    private List<Episode> episodes;

    public EpisodesAdapter(List<Episode> episodes) {
        this.episodes = episodes;
    }

    @NonNull
    @Override
    public EpisodesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new EpisodesViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.episodes_item_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull EpisodesViewHolder holder, int position) {
        holder.bindEpisode(episodes.get(position));
    }

    @Override
    public int getItemCount() {
        return episodes.size();
    }

    class EpisodesViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_date_episode;
        private TextView txt_num_episode;
        private TextView txt_name_episode;

        public EpisodesViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_date_episode = itemView.findViewById(R.id.txt_date_episode);
            txt_num_episode = itemView.findViewById(R.id.txt_num_episode);
            txt_name_episode = itemView.findViewById(R.id.txt_name_episode);
        }

        public void bindEpisode(Episode episode) {
            txt_name_episode.setText(episode.getName());
            txt_num_episode.setText("Season "+episode.getSeason()+" Episode "+ episode.getEpisode());
            txt_date_episode.setText(episode.getAirDate());
        }
    }
}
