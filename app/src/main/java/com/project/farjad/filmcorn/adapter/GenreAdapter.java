package com.project.farjad.filmcorn.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.model.Serial;

import java.util.List;

public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenreViewHolder> {
    private List<String> genres;

    public GenreAdapter(List<String> genres) {
        this.genres = genres;
    }

    @NonNull
    @Override
    public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new GenreViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.genre_item_layout,parent,false)) ;
    }

    @Override
    public void onBindViewHolder(@NonNull GenreViewHolder holder, int position) {
        holder.BindGenre(genres.get(position));
    }

    @Override
    public int getItemCount() {
        return genres.size();
    }

    public class GenreViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_genre_serial;

        public GenreViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_genre_serial = itemView.findViewById(R.id.txt_genre_serial);
        }

        public void BindGenre(String string){
            txt_genre_serial.setText(string);
        }
    }
}
