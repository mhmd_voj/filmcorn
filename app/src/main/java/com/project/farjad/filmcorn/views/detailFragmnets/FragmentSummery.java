package com.project.farjad.filmcorn.views.detailFragmnets;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.adapter.GenreAdapter;
import com.project.farjad.filmcorn.databinding.FragmentSummeryBinding;
import com.project.farjad.filmcorn.model.SerialDetail;

public class FragmentSummery extends Fragment {
    private FragmentSummeryBinding binding;
    private SerialDetail serialDetail;

    public FragmentSummery(SerialDetail serialDetail) {
        this.serialDetail = serialDetail;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSummeryBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        GenreAdapter genreAdapter = new GenreAdapter(serialDetail.getGenres());
        binding.rclGenres.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        binding.rclGenres.setAdapter(genreAdapter);
        if (serialDetail.getDescriptionSource()!=null){
            binding.txtDescriptionSource.setText(serialDetail.getDescriptionSource());
        }else {
            binding.btnSourceGo.setVisibility(View.GONE);
            binding.txtDescriptionSource.setVisibility(View.GONE);
            binding.textView5.setVisibility(View.GONE);
        }


        binding.txtDescriptionDetail.setText(
                String.valueOf(HtmlCompat.fromHtml(
                        serialDetail.getDescription(),
                        HtmlCompat.FROM_HTML_MODE_LEGACY
                ))
        );


        binding.btnSourceGo.setOnClickListener(c -> {
            String url = serialDetail.getDescriptionSource();
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        });
    }

}
