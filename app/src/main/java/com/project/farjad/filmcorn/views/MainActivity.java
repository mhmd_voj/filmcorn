package com.project.farjad.filmcorn.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.project.farjad.filmcorn.R;
import com.project.farjad.filmcorn.databinding.ActivityMainBinding;
import com.ss.bottomnavigation.events.OnSelectedItemChangeListener;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private FragmentTransaction transaction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        binding.bottomNavigation.setTypeface(ResourcesCompat.getFont(this, R.font.futura_light_font));

        binding.bottomNavigation.setOnSelectedItemChangeListener(i -> {
            switch (i) {
                case R.id.tab_home:
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container_main, new FragmentHome());
                    break;
                case R.id.tab_search:
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container_main, new FragmentSearch());
                    break;
                case R.id.tab_favorites:
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container_main, new FragmentFavorites());
                    break;
                case R.id.tab_info:
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container_main, new FragmentInfo());
                    break;
            }
            transaction.commit();
        });
    }
}