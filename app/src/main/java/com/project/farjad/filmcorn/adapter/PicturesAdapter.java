package com.project.farjad.filmcorn.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.project.farjad.filmcorn.R;

import java.util.List;

public class PicturesAdapter extends RecyclerView.Adapter<PicturesAdapter.PictureViewHolder> {
    private List<String> urlImages;

    public PicturesAdapter(List<String> urlImages) {
        this.urlImages = urlImages;
    }

    @NonNull
    @Override
    public PictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PictureViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.pictures_item_layout,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull PictureViewHolder holder, int position) {
        holder.simpleDraweeView.setImageURI(urlImages.get(position));
    }

    @Override
    public int getItemCount() {
        return urlImages.size();
    }

    class PictureViewHolder extends RecyclerView.ViewHolder{
        private SimpleDraweeView simpleDraweeView;

        public PictureViewHolder(@NonNull View itemView) {
            super(itemView);
            simpleDraweeView =itemView.findViewById(R.id.img_s_serial);
        }
    }
}
