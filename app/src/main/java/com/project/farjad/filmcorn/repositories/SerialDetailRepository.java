package com.project.farjad.filmcorn.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.project.farjad.filmcorn.model.SerialDetailResponse;
import com.project.farjad.filmcorn.network.ApiService;
import com.project.farjad.filmcorn.network.RetrofitService;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SerialDetailRepository {

    private RetrofitService retrofitService;

    public SerialDetailRepository() {
        retrofitService = ApiService.getRetrofit().create(RetrofitService.class);
    }

    public LiveData<SerialDetailResponse> getSerialFDetailsLiveData(String serialID){
        MutableLiveData<SerialDetailResponse> liveData = new MutableLiveData<>();
        retrofitService.getSerialDetail(serialID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<SerialDetailResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(SerialDetailResponse serialDetailResponse) {
                        liveData.setValue(serialDetailResponse);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
        return liveData;
    }
}
