package com.project.farjad.filmcorn.repositories;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.project.farjad.filmcorn.model.SerialListResponse;
import com.project.farjad.filmcorn.network.ApiService;
import com.project.farjad.filmcorn.network.RetrofitService;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public  class SerialsRepository {
    private RetrofitService retrofitService;

    public SerialsRepository() {
        retrofitService =ApiService.getRetrofit().create(RetrofitService.class);
    }

    public LiveData<SerialListResponse> getTvShows(int page){
        MutableLiveData<SerialListResponse> tvShowResponseMutableLiveData = new MutableLiveData<>();
        retrofitService.getMostPopularTvShows(page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<SerialListResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(SerialListResponse serialListResponse) {
                        tvShowResponseMutableLiveData.setValue(serialListResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("TAG", "onError: " +e.toString());
                    }
                });
        return tvShowResponseMutableLiveData;
    }

    public LiveData<SerialListResponse> getSearchTvShow(String str,int page){
        MutableLiveData<SerialListResponse> liveData = new MutableLiveData<>();
        retrofitService.getSearchTvShows(str,page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<SerialListResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(SerialListResponse serialListResponse) {
                        liveData.setValue(serialListResponse);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
        return liveData;
    }
}
